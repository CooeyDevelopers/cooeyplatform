//Sample code for Android app using the Cooey Platform provider API for lifesense.

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
	{
		View rootView = inflater.inflate(R.layout.device_list, container, false);

		
		mBleDeviceList = new ArrayList<LsDeviceInfo>();
		deviceTypeList=new ArrayList<String>();
		mScanButton = (Button)rootView.findViewById(R.id.scanBtn);
		mPairButton = (Button)rootView.findViewById(R.id.pairBtn);
		mDataButton = (Button)rootView.findViewById(R.id.Btn3);
		
		//Step 1
		mFactory = new CooeyBleDeviceFactory(new int [] {CooeyDeviceType.BP_METER});
		//Step 2
		mManager = (CooeyBleDeviceManager) mFactory.getBleDeviceManager();
		//Step 3
		mManager.initialize(new CooeyProfile(), getActivity());
		
		// Step 4 - Validate the connectivity
		//initialize a list view adapter to show the list of devices discovered.
		CooeyStatus cs = mManager.checkPlatformCapability();
		
		// Step 5 - Scan
		mScanButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mManager.searchDevicesForPairing(new CooeyDeviceSearchCallback() {
					
					@Override
					public void onDeviceFound(LsDeviceInfo arg0) {
						// TODO Auto-generated method stub
						if (!deviceExists(arg0.getDeviceName())) {
							mBleDeviceList.add(arg0);
						}
					}

				});
			}
		});
		// Step 6 - Pair the device
		mPairButton.setOnClickListener(new OnClickListener() {
					// from the list of devices detected, get the selected position and retrieve the device information
					@Override
					public void onClick(View v) {
						mManager.pairDevice( mBleDeviceList.get(0), new CooeyDevicePairCallback() {
							
							@Override
							public void onPairingResult(LsDeviceInfo arg0, int arg1) {
								// Pair the device
								mManager.registerPairedDevices(arg0);
							}
							
							@Override
							public void onDiscoverUserInfo(List arg0) {
								// TODO Auto-generated method stub
							}
						});
								
						
					}
				});
		//Step 7 - Start receiving data
		mDataButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mManager.receiveData(new CooeyDeviceDataReceiveCallback() {
					
					@Override
					public void onReceiveWeightdata(WeightData wd) {
						// TODO Auto-generated method stub
						// show the weight information onscreen
						
					}
					
					@Override
					public void onReceiveBPData(BPData bpd) {
						// TODO Auto-generated method stub
						// show the bp information onscreen
					}
				});
			}
		});
		
		
		return rootView;
	}
	
	private boolean deviceExists(String name) 
	{
		if(name==null || name.length()==0)
		{
			return false;
		}
		if(mBleDeviceList!=null && mBleDeviceList.size()>0)
		{
			for (int i = 0; i < mBleDeviceList.size(); i++)
			{
				if (mBleDeviceList.get(i)!=null && mBleDeviceList.get(i).getDeviceName()!=null && mBleDeviceList.get(i).getDeviceName().equals(name)) 
				{
					return true;
				}
			}
			return false;
		}
		else return false;
		
	}