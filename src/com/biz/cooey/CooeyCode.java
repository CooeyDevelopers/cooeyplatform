package com.biz.cooey;

public class CooeyCode {

	public static final int COOEY_OK = 1;
	public static final int COOEY_FAIL = 0;
	public static final int COOEY_ERROR = -1;
	public static final int COOEY_UNSUPPORTED = -3;
	
	public static final int COOEY_NOTHING = -2;
}
