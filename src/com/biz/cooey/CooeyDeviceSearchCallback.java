package com.biz.cooey;

import com.lifesense.ble.bean.LsDeviceInfo;


public abstract class CooeyDeviceSearchCallback {

	public abstract void onDeviceFound(LsDeviceInfo arg0);

}
