package com.biz.cooey;

public class CooeyStatus {

	private int status;
	private String message;
	
	public CooeyStatus(){
		this.status = CooeyCode.COOEY_NOTHING;
		this.message = "";
	}
	public int getStatus() {
		return status;
	}
	public CooeyStatus setStatus(int status) {
		this.status = status;
		return this;
	}
	public String getMessage() {
		return message;
	}
	public CooeyStatus setMessage(String message) {
		this.message = message;
		return this;
	}
	
}
