package com.biz.cooey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;


public class BPData {
	
	 
	  private String date;
	  private transient String deviceId; //prevent serialization
	  private String deviceSelectedUnit;
	  private String deviceSn;
	  @Expose private int diastolic;
	  @Expose private int systolic;
	  private float meanArterialPressure;
	  private String measurementPositionDetectionFlag;
	  @Expose @SerializedName("pulse") private float pulseRate;
	  private String pulseRateRangeDetectionFlags;
	  @Expose @SerializedName("irregularHeartBeat") private Boolean irregularPulseDetectionFlag;
	  
	  private int userId;
	  private long utc;
	  
	  @Expose private int patientId;
	  private String notes;
	  @Expose private String TID;
	  @Expose private String trackingId;
	  @Expose private long timeStamp;
	  
	  
	  public BPData() {}
	  
	  public String getDate()
	  {
	    return this.date;
	  }
	  
	  public String getDeviceId()
	  {
	    return this.deviceId;
	  }
	  
	  public String getDeviceSelectedUnit()
	  {
	    return this.deviceSelectedUnit;
	  }
	  
	  public String getDeviceSn()
	  {
	    return this.deviceSn;
	  }
	  
	  public int getDiastolic()
	  {
	    return this.diastolic;
	  }
	  
	 
	  
	  public float getMeanArterialPressure()
	  {
	    return this.meanArterialPressure;
	  }
	  
	  public String getMeasurementPositionDetectionFlag()
	  {
	    return this.measurementPositionDetectionFlag;
	  }
	  
	  
	  public float getPulseRate()
	  {
	    return this.pulseRate;
	  }
	  
	  public String getPulseRateRangeDetectionFlags()
	  {
	    return this.pulseRateRangeDetectionFlags;
	  }
	  
	  public int getSystolic()
	  {
	    return this.systolic;
	  }
	  
	  public int getUserId()
	  {
	    return this.userId;
	  }
	  
	  public long getUtc()
	  {
	    return this.utc;
	  }
	  
	  public void setDate(String paramString)
	  {
	    this.date = paramString;
	  }
	  
	  public void setDeviceId(String paramString)
	  {
	    this.deviceId = paramString;
	  }
	  
	  public void setDeviceSelectedUnit(String paramString)
	  {
	    this.deviceSelectedUnit = paramString;
	  }
	  
	  public void setDeviceSn(String paramString)
	  {
	    this.deviceSn = paramString;
	  }
	  
	  public void setDiastolic(int paramFloat)
	  {
	    this.diastolic = paramFloat;
	  }
	  
	 
	  
	  public void setMeanArterialPressure(float paramFloat)
	  {
	    this.meanArterialPressure = paramFloat;
	  }
	  
	  public void setMeasurementPositionDetectionFlag(String paramString)
	  {
	    this.measurementPositionDetectionFlag = paramString;
	  }
	  
	  public void setPulseRate(float paramFloat)
	  {
	    this.pulseRate = paramFloat;
	  }
	  
	  public void setPulseRateRangeDetectionFlags(String paramString)
	  {
	    this.pulseRateRangeDetectionFlags = paramString;
	  }
	  
	  public void setSystolic(int paramFloat)
	  {
	    this.systolic = paramFloat;
	  }
	  
	  public void setUserId(int paramInt)
	  {
	    this.userId = paramInt;
	  }
	  
	  public void setUtc(long paramLong)
	  {
	    this.utc = paramLong;
	  }

		public int getPatientId() {
			return patientId;
		}
	
		public void setPatientId(int patientId) {
			this.patientId = patientId;
		}

		public String getNotes() {
			return notes;
		}

		public void setNotes(String notes) {
			this.notes = notes;
		}

		public String getTID() {
			return TID;
		}

		public void setTID(String tID) {
			TID = tID;
		}

		public String getTrackingId() {
			return trackingId;
		}

		public void setTrackingId(String trackingId) {
			this.trackingId = trackingId;
		}

		public Boolean getIrregularPulseDetectionFlag() {
			return irregularPulseDetectionFlag;
		}

		public void setIrregularPulseDetectionFlag(
				Boolean irregularPulseDetectionFlag) {
			this.irregularPulseDetectionFlag = irregularPulseDetectionFlag;
		}

		public long getTimeStamp() {
			return timeStamp;
		}

		public void setTimeStamp(long timeStamp) {
			this.timeStamp = timeStamp;
		}
}
