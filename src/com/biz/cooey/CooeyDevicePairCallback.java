package com.biz.cooey;

import java.util.List;

import com.lifesense.ble.bean.LsDeviceInfo;

public abstract class CooeyDevicePairCallback {
	
	public abstract void onPairingResult(LsDeviceInfo arg0, int arg1);
	
	public abstract void onDiscoverUserInfo(List arg0);

}
