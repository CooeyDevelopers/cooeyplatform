package com.biz.cooey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeightData {

	
	  @Expose @SerializedName("bodyFat") private float BodyFatRatio;
	  @Expose @SerializedName("totalWater") private float BodyWaterRatio;
	  @Expose @SerializedName("boneDensity") private float BoneDensity;
	  @Expose @SerializedName("muscleMass") private float MuscleMassRatio;
	  private float VisceralFatLevel;
	  
	  private String broadcastId;
	  private String date;
	  private String deviceId;
	  @Expose @SerializedName("units") private String deviceSelectedUnit = "kg";
	  private String deviceSn;
	
	  private int impedanceStatus;
	
	  
	  @Expose @SerializedName("weight") private float weightKg;
	  private float weightLb;
	  private float weightSt;
	  private int weightStatus;
	  
	  @Expose @SerializedName("BMI") private float bmi;
	  private float imp;
	  @Expose private String TID;
	  @Expose private String trackingId;
	  @Expose private long timeStamp;
	  @Expose private int patientId;

	  
	  public String getTID() {
			return TID;
		}
		
		public void setTID(String tID) {
			TID = tID;
		}
		
		public String getTrackingId() {
			return trackingId;
		}
		
		public void setTrackingId(String trackingId) {
			this.trackingId = trackingId;
		}
		
		public long getTimeStamp() {
			return timeStamp;
		}
		
		public void setTimeStamp(long timeStamp) {
			this.timeStamp = timeStamp;
		}
		
		public int getPatientId() {
			return patientId;
		}
		
		public void setPatientId(int patientId) {
			this.patientId = patientId;
		}
		
	  public float getBodyFatRatio()
	  {
	    return this.BodyFatRatio;
	  }
	  
	  public float getBodyWaterRatio()
	  {
	    return this.BodyWaterRatio;
	  }
	  
	  public float getBoneDensity()
	  {
	    return this.BoneDensity;
	  }
	  
	  public String getBroadcastId()
	  {
	    return this.broadcastId;
	  }
	  
	  public String getDate()
	  {
	    return this.date;
	  }
	  
	  public String getDeviceId()
	  {
	    return this.deviceId;
	  }
	  
	  public String getDeviceSelectedUnit()
	  {
	    return this.deviceSelectedUnit;
	  }
	  
	  public String getDeviceSn()
	  {
	    return this.deviceSn;
	  }
	
	  
	  public int getImpedanceStatus()
	  {
	    return this.impedanceStatus;
	  }
	  
	  public float getMuscleMassRatio()
	  {
	    return this.MuscleMassRatio;
	  }
	  
	
	  public float getVisceralFatLevel()
	  {
	    return this.VisceralFatLevel;
	  }
	  
	  public int getWeightStatus()
	  {
	    return this.weightStatus;
	  }
	  
	  public void setBodyFatRatio(float paramFloat)
	  {
	    this.BodyFatRatio = paramFloat;
	  }
	  
	  public void setBodyWaterRatio(float paramFloat)
	  {
	    this.BodyWaterRatio = paramFloat;
	  }
	  
	  public void setBoneDensity(float paramFloat)
	  {
	    this.BoneDensity = paramFloat;
	  }
	  
	  public void setBroadcastId(String paramString)
	  {
	    this.broadcastId = paramString;
	  }
	  
	  public void setDate(String paramString)
	  {
	    this.date = paramString;
	  }
	  
	  public void setDeviceId(String paramString)
	  {
	    this.deviceId = paramString;
	  }
	  
	  public void setDeviceSelectedUnit(String paramString)
	  {
	    this.deviceSelectedUnit = paramString;
	  }
	  
	  public void setDeviceSn(String paramString)
	  {
	    this.deviceSn = paramString;
	  }
	  
	 
	  public void setImpedanceStatus(int paramInt)
	  {
	    this.impedanceStatus = paramInt;
	  }
	  
	  public void setMuscleMassRatio(float paramFloat)
	  {
	    this.MuscleMassRatio = paramFloat;
	  }
	  
	
	  
	  public void setVisceralFatLevel(float paramFloat)
	  {
	    this.VisceralFatLevel = paramFloat;
	  }
	
	  public void setWeightStatus(int paramInt)
	  {
	    this.weightStatus = paramInt;
	  }

	
	public double getBmi() {
		return bmi;
	}

	public void setBmi(float bmi) {
		this.bmi = bmi;
	}

	public double getImp() {
		return imp;
	}

	public void setImp(float imp) {
		this.imp = imp;
	}

	

	public double getWeightKg() {
		return weightKg;
	}

	public void setWeightKg(float weightKg) {
		this.weightKg = weightKg;
	}

	public double getWeightSt() {
		return weightSt;
	}

	public void setWeightSt(float weightSt) {
		this.weightSt = weightSt;
	}

	public double getWeightLb() {
		return weightLb;
	}

	public void setWeightLb(float weightLb) {
		this.weightLb = weightLb;
	}
	  
}
