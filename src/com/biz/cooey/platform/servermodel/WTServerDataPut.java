package com.biz.cooey.platform.servermodel;

import com.google.gson.annotations.Expose;

public class WTServerDataPut {

	
	@Expose public float weight;
	@Expose public float BMI;
	@Expose public float bodyFat = 0;
	@Expose public float muscleMass;
	@Expose public float totalWater;
	@Expose public float boneDensity;
	@Expose public String units = "Kg";
	@Expose public int patientId;
	@Expose public String TID;
	@Expose public String trackingId;
	@Expose public long timeStamp;
}
