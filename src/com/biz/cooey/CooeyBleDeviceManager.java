package com.biz.cooey;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.format.Time;
import android.util.Log;

import com.biz.cooey.platform.servermodel.WTServerDataPut;
import com.google.gson.GsonBuilder;
import com.lifesense.ble.LsBleManager;
import com.lifesense.ble.PairCallback;
import com.lifesense.ble.ReceiveDataCallback;
import com.lifesense.ble.SearchCallback;
import com.lifesense.ble.bean.BloodPressureData;
import com.lifesense.ble.bean.LsDeviceInfo;
import com.lifesense.ble.bean.SexType;
import com.lifesense.ble.bean.UnitType;
import com.lifesense.ble.bean.WeightData_A2;
import com.lifesense.ble.bean.WeightData_A3;
import com.lifesense.ble.bean.WeightUserInfo;
import com.lifesense.ble.commom.BroadcastType;
import com.lifesense.ble.commom.DeviceType;
import com.lifesense.fat.FatPercentage;

public class CooeyBleDeviceManager extends BleDeviceManager {

	private Boolean isInitialized = false;
	private LsBleManager mLsBleManager = null;
	private List<DeviceType> mSuppDevices = null;
	private String mProfJson = "";
	//HttpClient httpClient = null;
	private SendJsonToCooeyTask cooeySrvConnProf = null, cooeySrvConnBPData = null, cooeySrvConnWEData = null;
	private CooeyProfile activeProfile = null;
	private int activePatientId = 0;
	
	public CooeyBleDeviceManager(List<DeviceType> activeDevices){
		mSuppDevices = new ArrayList<DeviceType>(activeDevices);
		cooeySrvConnProf = new SendJsonToCooeyTask(new SendToCooeyProfileResponse());
		//cooeySrvConnBPData = new SendJsonToCooeyTask(new SendToCooeyBPDataResponse());
		//cooeySrvConnWEData = new SendJsonToCooeyTask(new SendToCooeyWEDataResponse());
	}
	
	public static String deviceTypeToName(String type){
		
		switch(type){
			case "08": return "BP METER";
			case "01": return "WEIGHT & FAT SCALE";
			case "02": return "A3 WEIGHT & FAT SCALE";
			case "NOBEEF": return "Absent";
			default: return "";
		}
		
	}
	private int getAge(String dob){
		
		
		return 35;
	}
	
	public Boolean doesSupportBLE(){
		return mLsBleManager.isSupportLowEnergy();
		
	}
	
	public CooeyStatus initialize(CooeyProfile profile, Context arg1) {
		
		if(isInitialized == true) return new CooeyStatus().setStatus(CooeyCode.COOEY_OK).setMessage("");
		
		shutdown();
		mLsBleManager=LsBleManager.newInstance();
		isInitialized = mLsBleManager.initialize(arg1);
		mLsBleManager.stopDataReceiveService();
		
		int age = getAge(profile.getDob());
		WeightUserInfo weight=new WeightUserInfo();
        weight.setAge(age);
        //weight.setAthleteActivityLevel(2);
        weight.setAthlete(false);
        weight.setHeight(profile.getHeight()/100f); //height in meters
        if(profile.getGender().equalsIgnoreCase("male"))
        	weight.setSex(SexType.MALE);
        if(profile.getGender().equalsIgnoreCase("female"))
        	weight.setSex(SexType.FEMALE);
        if(profile.getGender().equalsIgnoreCase("male athletic")){
        	weight.setSex(SexType.MALE);
        	weight.setAthlete(true);
        }
        if(profile.getGender().equalsIgnoreCase("female athletic")){
        	weight.setSex(SexType.FEMALE);
        	weight.setAthlete(true);
        }        
        weight.setProductUserNumber(1);
        weight.setUnit(UnitType.UNIT_KG);
        //weight.setGoalWeight(88);
        //weight.setWaistline(68);
        //weight.setDeviceId("28DFE6F92BC2");
        weight.setMemberId("1");
        weight.setProductUserNumber(1);
        
        //weight.set
        mLsBleManager.setProductUserInfo(weight);
        
		//httpClient = new DefaultHttpClient();
		
		profile.settId("tid");
		profile.setTrackingId("track");
		mProfJson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(profile);
		
		activeProfile = profile;
		if(profile.getEmail() == "" || profile.getFirstName() == "" ||profile.getDob() == ""){
			return new CooeyStatus().setStatus(CooeyCode.COOEY_ERROR).setMessage("Profile details are mandatory");
		}
		//test
		//mProfJson = "{\"firstName\":\"Test\",\n \"lastName\":\"Test\",\n \"mobileNumber\":\"9845741130\",\n \"area\" :\"Test\",\n \"profilePic\" :\"Test\",\n \"gender\":\"M\",\n \"DOB\":\"1981-01-01\",\n \"email\":\"Test\", \n \"city\":\"Test\",\n \"country\":\"Test\",\n \"bloodGroup\":\"Test\", \n \"TID\":\"Test\",\n \"trackingId\":\"dddd\"}";
		cooeySrvConnProf.execute(mProfJson);
		return new CooeyStatus().setStatus(CooeyCode.COOEY_OK).setMessage("");
	}

	@Override
	public CooeyStatus checkPlatformCapability() {
		
		if(!isInitialized || mLsBleManager == null) 
			return new CooeyStatus().setStatus(CooeyCode.COOEY_ERROR).setMessage("Platform not initialized or shutdown was called");
		
		if(!mLsBleManager.isOpenBluetooth())
			return new CooeyStatus().setStatus(CooeyCode.COOEY_FAIL).setMessage("Bluetooth turned off");
		
		if(!mLsBleManager.isSupportLowEnergy()){
			return new CooeyStatus().setStatus(CooeyCode.COOEY_UNSUPPORTED).setMessage("No support for low energy bluetooth");
		}
		return new CooeyStatus().setStatus(CooeyCode.COOEY_OK).setMessage("");
	}

	@Override
	public void stopSearchForDevices() {
		mLsBleManager.stopSearch();
	}
	
	@Override
	public void searchDevicesForPairing(final CooeyDeviceSearchCallback sc) {
	
		mLsBleManager.stopDataReceiveService();
		try{
			mLsBleManager.searchLsDevice(new SearchCallback() {
				
				@Override
				public void onSearchResults(LsDeviceInfo arg0) {
					//CooeyDeviceInfo cdi = (CooeyDeviceInfo) arg0;
					sc.onDeviceFound(arg0);
					
				}
			}, mSuppDevices, BroadcastType.PAIR);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
		
	@Override
	public CooeyStatus registerPairedDevice(LsDeviceInfo pdev) {
		if( !mLsBleManager.addMeasureDevice(pdev) )
		{
			return new CooeyStatus().setStatus(CooeyCode.COOEY_FAIL).setMessage("Failed to register the Paired device");
		}
		mLsBleManager.stopDataReceiveService();
		return new CooeyStatus().setStatus(CooeyCode.COOEY_OK).setMessage("");
	}

	@Override
	public CooeyStatus shutdown() {
		// TODO Auto-generated method stub
		if(mLsBleManager == null) return new CooeyStatus().setStatus(CooeyCode.COOEY_ERROR).setMessage("calling shutdown before init/shutdown already called");
		
		Boolean ret = mLsBleManager.setMeasureDevice(new ArrayList<LsDeviceInfo>());//just clear the internal list of devices
		mLsBleManager.stopDataReceiveService();
		//mLsBleManager = null;
		return new CooeyStatus().setStatus(CooeyCode.COOEY_OK).setMessage("");
	}
	
	@Override
	public CooeyStatus deRegisterAllDevices() {
		if(mLsBleManager == null) return new CooeyStatus().setStatus(CooeyCode.COOEY_OK).setMessage("");
		mLsBleManager.stopDataReceiveService();
		Boolean ret = mLsBleManager.setMeasureDevice(new ArrayList<LsDeviceInfo>());//just clear the internal list of devices
		return new CooeyStatus().setStatus(CooeyCode.COOEY_OK).setMessage("");
	}
	
	@Override
	public CooeyStatus deRegisterPairedDevices(LsDeviceInfo arg0) {

		return null;
	}

	public void bindUserProfileWithName(int profileNumber, String name){
		if(name == "") name = "unknown";
		mLsBleManager.bindDeviceUser(profileNumber, name);
		
	}
	/*
	public void pairDeviceWithProfile(LsDeviceInfo de, final CooeyDevicePairCallback pc) {
		mLsBleManager.stopSearch();
		
		LsDeviceInfo lsDevice=new LsDeviceInfo();
		lsDevice.setDeviceName(de.getDeviceName());
		lsDevice.setBroadcastID(de.getBroadcastID());
		lsDevice.setDeviceType(de.getDeviceType());
		lsDevice.setProtocolType(de.getProtocolType());
		lsDevice.setModelNumber(de.getModelNumber());
		
		mLsBleManager.startPairing(lsDevice, new PairCallback() {
			
			@Override
			public void onPairResults(LsDeviceInfo arg0, int status) {
				pc.onPairingResult( arg0, status);
			}
			
			@Override
			public void onDiscoverUserInfo(List arg0) {
				
				pc.onDiscoverUserInfo(arg0);
				DeviceUserInfo d = (DeviceUserInfo) arg0.get(0);
				
				mLsBleManager.bindDeviceUser(1, "Ashwath");
			}
		});

	}
	*/
	@Override
	public void pairDevice(LsDeviceInfo de, final CooeyDevicePairCallback pc) {
		mLsBleManager.stopSearch();
		
		LsDeviceInfo lsDevice=new LsDeviceInfo();
		lsDevice.setDeviceName(de.getDeviceName());
		lsDevice.setBroadcastID(de.getBroadcastID());
		lsDevice.setDeviceType(de.getDeviceType());
		lsDevice.setProtocolType(de.getProtocolType());
		lsDevice.setModelNumber(de.getModelNumber());
		lsDevice.setPairStatus(1);
		
		
		mLsBleManager.startPairing(lsDevice, new PairCallback() {
			
			@Override
			public void onPairResults(LsDeviceInfo arg0, int status) {
				pc.onPairingResult( arg0, status);
			}
			
			@Override
			public void onDiscoverUserInfo(List arg0) {
				
				pc.onDiscoverUserInfo(arg0);
			}
		});
	}
	
	@Override
	public int getMaxUserProfilesForDevice(LsDeviceInfo de){
		
		return de.getMaxUserQuantity();
	}
	
	/*public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}*/
	
	private float round2(double d){
		try {
			DecimalFormat twoDForm = new DecimalFormat("#.##");
		    Float t = Float.valueOf(twoDForm.format(d));
		    return t;
		} catch (Exception e) {
			return 0f;
		}
		
	}
	private float round1(double d){
		try {
			DecimalFormat twoDForm = new DecimalFormat("#.#");
			Float t = Float.valueOf(twoDForm.format(d));
			return t;
		} catch (Exception e) {
			return 0f;
		}
		
	}
	
	private WeightData getWeightDataA2Parameters( WeightData_A2 wData_A2 ){
		
		FatPercentage fp = FatPercentage.getInstance(FatPercentage.fda);
		
		WeightData wd = new WeightData();
		wd.setWeightKg(round1(wData_A2.getWeight()));
		double lbw = wData_A2.calculateLBValueWithKGValue(wData_A2.getWeight());
		wd.setWeightLb(round1(lbw));

		int age = 0;
		float height = activeProfile.getHeight() / 100f;
		float bmi = (float) fp.getBmi(wData_A2.getWeight(), height);
		wd.setBmi(round2(bmi));
		
		float imp = (float) fp.getImp((int) wData_A2.getResistance_2());
		wd.setImp(imp);
		
		float fat = (float) fp.getFat(FatPercentage.Male, imp, 25, bmi, false);
		wd.setBodyFatRatio((float) (round2(fat)));
		
		float bone = (float) fp.getBone(FatPercentage.Male, imp, 25, bmi, false);
		wd.setBoneDensity((float) round2(bone));

		float mus = (float) fp.getMuscle(FatPercentage.Male, imp, 25, bmi, false);
		wd.setMuscleMassRatio((float) round2(mus));
		
		float water = (float) fp.getBodyWater(FatPercentage.Male, imp, 25, bmi, false);
		wd.setBodyWaterRatio((float) round2(water));
		
		Time now = new Time();
		now.setToNow();
		
		wd.setDate(now.format("%d-%m-%Y %H:%M:%S"));//wData_A2.getDate());
		wd.setDeviceSelectedUnit(wData_A2.getDeviceSelectedUnit());
		wd.setWeightStatus(wData_A2.getWeightStatus());
		
		return wd;		
	}
	
	private WeightData getWeightDataA3Parameters( WeightData_A3 wData_A3 ){
		
		FatPercentage fp = FatPercentage.getInstance(FatPercentage.fda);
		
		WeightData wd = new WeightData();
		wd.setWeightKg(round1(wData_A3.getWeight()));
		
		//double lbw = wData_A3.calculateLBValueWithKGValue(wData_A3.getWeight());
		//wd.setWeightLb(round1(lbw));

		int age = 0;
		float height = activeProfile.getHeight() / 100f;
		float bmi = (float) fp.getBmi(wData_A3.getWeight(), height);
		wd.setBmi(round2(bmi));
		
		float imp = (float) fp.getImp((int) wData_A3.getImpedance());
		wd.setImp(imp);
		
		float fat = (float) fp.getFat(FatPercentage.Male, imp, 25, bmi, false);
		wd.setBodyFatRatio((float) (round2(fat)));
		
		float bone = (float) fp.getBone(FatPercentage.Male, imp, 25, bmi, false);
		wd.setBoneDensity((float) round2(bone));

		float mus = (float) fp.getMuscle(FatPercentage.Male, imp, 25, bmi, false);
		wd.setMuscleMassRatio((float) round2(mus));
		
		float water = (float) fp.getBodyWater(FatPercentage.Male, imp, 25, bmi, false);
		wd.setBodyWaterRatio((float) round2(water));
		
		Time now = new Time();
		now.setToNow();
		
		wd.setDate(now.format("%d-%m-%Y %H:%M:%S"));//wData_A2.getDate());
		wd.setDeviceSelectedUnit(wData_A3.getDeviceSelectedUnit());
		//wd.setWeightStatus(wData_A3.getWeightStatus());
		
		return wd;		
	}
	
	
	@Override
	public CooeyStatus receiveData(final CooeyDeviceDataReceiveCallback drc) {
		if(mLsBleManager == null) 
			return new CooeyStatus().setStatus(CooeyCode.COOEY_ERROR).setMessage("Platform not initialized or shutdown was called");
		if(drc == null)
			return new CooeyStatus().setStatus(CooeyCode.COOEY_ERROR).setMessage("Null callback passed");
		
		//mLsBleManager.stopSearch();
		mLsBleManager.stopDataReceiveService();
		
		//calltest();
		
		mLsBleManager.startDataReceiveService(new ReceiveDataCallback() 
		{
			public void onReceiveWeightDta_A2(WeightData_A2 wData_A2)
			{
				WeightData wd = getWeightDataA2Parameters(wData_A2);				
				drc.onReceiveWeightdata(wd);
				
				/*
				WTServerDataPut wtsrv = new WTServerDataPut();
				wtsrv.weight = (long) wd.getWeightKg();
				wtsrv.BMI = (long) wd.getBmi();
				if(wd.getBodyFatRatio() > 0) {
					wtsrv.bodyFat = (long)wd.getBodyFatRatio();
				};
				wtsrv.boneDensity = (long) wd.getBoneDensity();
				wtsrv.muscleMass = (long) wd.getMuscleMassRatio();
				wtsrv.patientId = getActiveProfile().getPatientId();
				wtsrv.TID = "androidapp";
				wtsrv.trackingId = "android";
				wtsrv.timeStamp = new Date().getTime();
				wtsrv.totalWater = (long) wd.getBodyWaterRatio();
				
				cooeySrvConnWEData = new SendJsonToCooeyTask(new SendToCooeyWEDataResponse());
				cooeySrvConnWEData.execute("");*/
			};	
			
			public void onReceiveWeightData_A3(WeightData_A3 wData)
			{
				WeightData wd = getWeightDataA3Parameters(wData);				
				drc.onReceiveWeightdata(wd);
				/*
				WeightData wd = new WeightData();
				wd.setWeightKg((float)wData.getWeight());
				//wd.setUserId(wData.getUserId());
				drc.onReceiveWeightdata(wd);
				cooeySrvConnWEData = new SendJsonToCooeyTask(new SendToCooeyWEDataResponse());
				cooeySrvConnWEData.execute("");*/
			}		
			
			public void onReceiveBloodPressureData(BloodPressureData bpData)
			{
				BPData bpd = new BPData();
				Time now = new Time();
				now.setToNow();
				
				bpd.setDate(now.format("%d-%m-%Y %H:%M:%S"));				
				//bpd.setDate(bpData.getDate());
				bpd.setSystolic((int) bpData.getSystolic());
				bpd.setDiastolic((int) bpData.getDiastolic());
				bpd.setUserId(bpData.getUserId());
				bpd.setUtc(bpData.getUtc());
				bpd.setPulseRate(bpData.getPulseRate());
				bpd.setTimeStamp(new Date().getTime());
				bpd.setIrregularPulseDetectionFlag(bpData.getIrregularPulseDetectionFlag() == "1"? true: false);
				bpd.setTID("vendor:android");
				bpd.setTrackingId("vendor:android");
				drc.onReceiveBPData(bpd);
				
				if(activePatientId == 0){
					activePatientId = activeProfile.getPatientId();
				}			
				
				bpd.setPatientId(activePatientId);
				String mBpJson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(bpd);
				Log.d(" BP DATA TO POST ", mBpJson);
				new SendJsonToCooeyTask(new SendToCooeyCallbackOnResponse() {
					
					@Override
					public String getUrl() {
						return "http://api.cooey.co.in/ehealth/v1/actvity/bp";
					}
					
					@Override
					public void callme(String resp, int responseCode) {
						// TODO Auto-generated method stub
						int x = 0;
					}

					@Override
					public void callmeWithContext(String resp, Context context) {
						// TODO Auto-generated method stub
						
					}
				}).execute(mBpJson);
				
				/*cooeySrvConnBPData = new SendJsonToCooeyTask(new SendToCooeyBPDataResponse());
				bpd.setPatientId(activePatientId);
				bpd.setNotes("");
				bpd.setTID("tid");
				bpd.setTrackingId("track");
				
				cooeySrvConnBPData.execute(
						new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(bpd)
				);*/
			}	
			
		});
		return new CooeyStatus().setStatus(CooeyCode.COOEY_OK).setMessage("");
	}
	
	private class SendToCooeyProfileResponse implements SendToCooeyCallbackOnResponse{
		private String url = "http://manage.cooey.co.in/ehealth/v1/profile/patient";
		@Override
		public void callme(String resp, int code) {
			try {
				JSONObject jo = new JSONObject(resp);
				activePatientId = (int) jo.get("patientId");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		@Override
		public String getUrl() {
			return url;
		}
		@Override
		public void callmeWithContext(String resp, Context context) {
			// TODO Auto-generated method stub
			
		}
	}
	
	private class SendToCooeyBPDataResponse implements SendToCooeyCallbackOnResponse{
		//manage.cooey.co.in
		public String url = "http://manage.cooey.co.in/ehealth/v1/actvity/bp";
		@Override
		public void callme(String resp, int code) {
			try {
				JSONObject jo = new JSONObject(resp);
				if((Boolean)jo.get("operationStatus") == true){
					//log failure
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		@Override
		public String getUrl() {
			return url;
		}
		@Override
		public void callmeWithContext(String resp, Context context) {
			// TODO Auto-generated method stub
			
		}
	}
	
	private class SendToCooeyWEDataResponse implements SendToCooeyCallbackOnResponse{
		public String url = "http://api.cooey.co.in/ehealth/v1/actvity/weight";
		@Override
		public void callme(String resp, int code) {
			
			
		}
		@Override
		public String getUrl() {
			return url;
		}
		@Override
		public void callmeWithContext(String resp, Context context) {
			// TODO Auto-generated method stub
			
		}
	}

	
}
